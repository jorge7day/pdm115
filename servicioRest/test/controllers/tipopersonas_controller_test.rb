require 'test_helper'

class TipopersonasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tipopersona = tipopersonas(:one)
  end

  test "should get index" do
    get tipopersonas_url
    assert_response :success
  end

  test "should get new" do
    get new_tipopersona_url
    assert_response :success
  end

  test "should create tipopersona" do
    assert_difference('Tipopersona.count') do
      post tipopersonas_url, params: { tipopersona: { nombretipo: @tipopersona.nombretipo } }
    end

    assert_redirected_to tipopersona_url(Tipopersona.last)
  end

  test "should show tipopersona" do
    get tipopersona_url(@tipopersona)
    assert_response :success
  end

  test "should get edit" do
    get edit_tipopersona_url(@tipopersona)
    assert_response :success
  end

  test "should update tipopersona" do
    patch tipopersona_url(@tipopersona), params: { tipopersona: { nombretipo: @tipopersona.nombretipo } }
    assert_redirected_to tipopersona_url(@tipopersona)
  end

  test "should destroy tipopersona" do
    assert_difference('Tipopersona.count', -1) do
      delete tipopersona_url(@tipopersona)
    end

    assert_redirected_to tipopersonas_url
  end
end
