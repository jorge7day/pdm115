json.extract! tipopersona, :id, :nombretipo, :created_at, :updated_at
json.url tipopersona_url(tipopersona, format: :json)
