class TipopersonasController < ApplicationController
  before_action :set_tipopersona, only: [:show, :edit, :update, :destroy]

  # GET /tipopersonas
  # GET /tipopersonas.json
  def index
    @tipopersonas = Tipopersona.all
  end

  # GET /tipopersonas/1
  # GET /tipopersonas/1.json
  def show
  end

  # GET /tipopersonas/new
  def new
    @tipopersona = Tipopersona.new
  end

  # GET /tipopersonas/1/edit
  def edit
  end

  # POST /tipopersonas
  # POST /tipopersonas.json
  def create
    @tipopersona = Tipopersona.new(tipopersona_params)

    respond_to do |format|
      if @tipopersona.save
        format.html { redirect_to @tipopersona, notice: 'Tipopersona was successfully created.' }
        format.json { render :show, status: :created, location: @tipopersona }
      else
        format.html { render :new }
        format.json { render json: @tipopersona.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tipopersonas/1
  # PATCH/PUT /tipopersonas/1.json
  def update
    respond_to do |format|
      if @tipopersona.update(tipopersona_params)
        format.html { redirect_to @tipopersona, notice: 'Tipopersona was successfully updated.' }
        format.json { render :show, status: :ok, location: @tipopersona }
      else
        format.html { render :edit }
        format.json { render json: @tipopersona.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tipopersonas/1
  # DELETE /tipopersonas/1.json
  def destroy
    @tipopersona.destroy
    respond_to do |format|
      format.html { redirect_to tipopersonas_url, notice: 'Tipopersona was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tipopersona
      @tipopersona = Tipopersona.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tipopersona_params
      params.require(:tipopersona).permit(:nombretipo)
    end
end
