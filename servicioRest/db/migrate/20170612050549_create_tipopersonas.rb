class CreateTipopersonas < ActiveRecord::Migration[5.1]
  def change
    create_table :tipopersonas do |t|
      t.string :nombretipo

      t.timestamps
    end
  end
end
