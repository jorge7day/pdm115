# PDM PROJECT

## Requerimientos
- Node: [https://nodejs.org/en/download](https://nodejs.org/en/download)
- Phonegap (npm install -g phonegap)
- Bower (npm install -g bower)
- Gulp (npm install -g gulp)
- Cambiar la línea 8 de gulpfile.js, añadiendo _/www_ despúes de _dist_

### ¿Cómo iniciar?
1. Descargar & descomprimir: [https://lookaside.fbsbx.com/file/nunjucks-starter-kit-master%20.zip](https://lookaside.fbsbx.com/file/nunjucks-starter-kit-master%20.zip?token=AWzq4H1XucZgXJwBpcw2hCoLjoVivTEnHjJkEvDxKAZ32yL8k1fqwEFBy4Ig46SVa5BnCZlln_MGC0IwSIm8PvRwqMAvsDMLApwzcmYnD2elFIv_sEHp6g_LSL1FFsyyREtqNGyPJDjdFKpoGfw-Zx-jCS3n6AgKKGf8EpYpZN8YhJ8ufgrwi1uIZvxeyIKGy2M)
- Instalar dependencias: `npm i`
- Ejecutar servidor: `gulp auto`
- Solo renderizar vistas: `gulp`
- Intenta hacer cambios en la carpeta `src/pages` y tu navegador se actualizará automáticamente, mostrando el cambio que haz hecho.

![https://youtu.be/QGQFUesQXwk](thumbnail.jpg)

Mira el video de YouTube: [https://youtu.be/QGQFUesQXwk](https://youtu.be/QGQFUesQXwk)

> *Ejecuta `gulp minify` si quieres minimizar tus archivos html dentro de la carpeta __dist/www__*

---

# ¿Por qué?
- ¡Funciona desde el principio!
- Plantilla padre incluida (usando framework7):
  - `src/pages` para el contenido de las plantillas hijas
  - `src/templates` para las plantillas padres
  - La salida es HTML plano (guardada en la carpeta `dist/www`)
- No necesita servidor
- ¡Minimizador incluído! usa `gulp minify`

## Carpeta de Plantillas
La estructura del proyecto es el siguiente:

- `dist` : Este directorio contiene los archivos finales, los que serán publicados, donde estará el proyecto de **phonegap**
- `dist/www` : Las librerías básicas y los HTML que se usarán en el proyecto de phonegap.
- `src` : Archivos usados para desarrollar las páginas
- `src/pages` : Páginas (HTML) para la aplicación, llamadas _"hijas"_. Todo lo que aquí se encuentre será renderizado hacia `dist/www`.
- `src/templates` : Archivos de plantillas (`.njk`).  Lo que aquí se encuentre, **no será enviado a *dist/www* directamente**, sino solo servirán como base para la renderización de los HTML de la carpeta anterior.

## Flujo de trabajo de ejemplo
__A. Sin auto-render__

1. Edita las páginas en `src/pages`
- Ejecuta `gulp`
- Publica los cambios de `dist` al repositorio


__B. Con auto-render__

1. Ejecuta `gulp watch`
- A medida editas en `src/pages`, gulp hará las renderizaciones en background en `dist/www`. ***Necesitas recargar el navegador para ver los cambios***
- Publica `dist` al repositorio


__C. Con auto-render & auto-reload (via browsersync)__

1. Ejecuta `gulp auto`
- Abrirá [http://localhost:3000]`http://localhost:3000` en tu navegador.
- Usando este flujo de trabajo, cuando edites los archivos en `src`, `dist` será actualizado y tu navegador mostrará los cambios automáticamente en (`http://localhost:3000`).



---
Nunjucks official docs: [https://mozilla.github.io/nunjucks]([https://mozilla.github.io/nunjucks)
